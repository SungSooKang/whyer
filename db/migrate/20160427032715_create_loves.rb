class CreateLoves < ActiveRecord::Migration
  def change
    create_table :loves do |t|
      t.references :adorable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
