class AddReceiverToLoves < ActiveRecord::Migration
  def change
    add_reference :loves, :receiver, index: true, foreign_key: true
  end
end
