class AddIsCheckedToLoves < ActiveRecord::Migration
  def change
    add_column :loves, :is_checked, :boolean, default: false, null: false
  end
end
